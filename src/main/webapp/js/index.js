/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function login() {
    var form = $('#form_index');
    $.ajax({
        type: form.attr('method'),
        url: 'Login',
        data: form.serialize(),
        success: function (data) {
            console.log(data);
            var json = JSON.parse(data);
            //muestraMensaje(data);
            if (json.status === 'ok') {
                location.href = 'ver_mejoras.jsp';
            } else {
                alert(json.mensaje);
            }
        },
        error: function () {
            alert("Error Login");
        }
    });

}