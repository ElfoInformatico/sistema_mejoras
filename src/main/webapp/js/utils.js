var Utils = (function () {

    var name = "Utils";
    var description = "Módulo para las utilidades genéricas de la app";

    /**
     * Permite hacer un Ajax standard.
     * @param {string} url
     * @param {Object} data
     * @param {string} type
     * @param {function} [beforeSend]
     * @returns {jqXHR} Petición Ajax.
     */
    var callRequest = function (url, data, type, beforeSend) {
        
        return createRequest(url, prepareDataAjax(data), type, 'JSON', beforeSend);
    };


    var sendFile = function (url, input, subfix, beforeSend) {

        var data = new FormData();

        data.append("data", prepareDataAjax({subfix: subfix}).data);
        data.append("file", input[0].files[0]);

        return createRequest(url, data, 'POST', undefined, beforeSend, false, false);
    };


    var createRequest = function (url, data, type, dataType, beforeSend, contentType,
            processData) {

        beforeSend = beforeSend || function () {};

        processData = processData === undefined ? true : contentType;

        return $.ajax({
            url: url,
            method: type,
            type: type,
            dataType: dataType,
            cache: false,
            data: data,
            beforeSend: beforeSend,
            contentType: contentType,
            processData: processData
        });
    };

    /**
     * Muestra una notificacion de cargando en pantalla.
     * 
     * @param {String} [msg] Mensaje a mostrar. Por defecto: 'Estamos procesando tu solicitud.'
     * @param {String} [previous] ID de la anterior notificacion, si la
     * tiene se quita la anterior antes de poner la nueva.
     * 
     * @returns {String} guid de la notificacion.
     */
    var showLoading = function (msg, previous) {
        msg = msg || "Estamos procesando tu solicitud.";

        return showNotification(msg, 'loading', undefined, previous);
    };

    /**
     * Muestra notificación en pantalla
     * 
     * @param {String} msg Mensaje que se desea mostrar en pantalla.
     * @param {String} type Los posibles tipos son: 'loading', 'success', 'error'
     * @param {int} [delay] Tiempo en segundos para ocultar.
     * @param {String} [previous] ID de la anterior notificacion, si la
     * tiene se quita la anterior antes de poner la nueva.
     * 
     * @returns {String} guid de la notificacion.
     */
    var showNotification = function (msg, type, delay, previous) {
        delay = delay || -1;
        previous = previous || "";

        var classType = type === 'error' ? 'alert-danger' : 'alert-success';

        if (type === 'success') {
            msg = '<i class="fa fa-check"></i> ' + msg;
            classType = 'alert-success';
        }

        if (type === 'error') {
            msg = '<i class="fa fa-close"></i> ' + msg;
            classType = 'alert-danger';
        }

        if (type === 'loading') {
            msg = '<i class="fa fa-refresh fa-spin"></i> ' + msg;
            classType = 'alert-info';
        }

        var guid = guidGenerator();
        var html = '<div id="' + guid + '" class="myAlert alert ' + classType + ' fade in"> ' +
                '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong class="h4">' +
                msg + '</strong></div>';

        $('div[class="container"]').prepend(html);

        var notification = $('#' + guid);
        notification.show();
        if (previous !== "") {
            $('#' + previous).hide(550);
        }

        if (delay !== -1) {
            setTimeout(function () {
                notification.hide(550);
            }, delay * 1000);

        }

        return guid;
    };

    /**
     * Genera ID unico.
     * 
     * @returns {String} el id generado.
     */
    var guidGenerator = function () {
        var S4 = function () {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        };
        return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
    };

    /**
     * metodo para validar si un objeto está disponible para ser usado, en caso 
     * contrario se pone e un intervalo hasta que esté disponible o pasen 30seg
     * @param {type} obj
     * @param {type} callback
     * @param {type} father
     * @returns {object} el objeto solicitado
     */
    var isObjAvailable = function (obj, callback, father) {
        father = father || window;
        var validation = function () {
            return !$.isEmptyObject(father[obj]);
        };

        intervalValidation(validation, function () {
            callback(father[obj]);
        });
    };

    /**
     * Convierte una los datos envias en el formato que recibe el servidor.
     * 
     * @param {Object} data es un objeto con los datos a enviar al servidor.
     * @returns {Object} con formato para el servidor.
     */
    var prepareDataAjax = function (data) {
        return {data: JSON.stringify(data)};
    };


    /**
     * Permite imprimir los archivos de excel en una nueva ventana.
     * 
     * @param {type} context
     * @param {type} url
     * @param {type} [parms]
     */
    var printReportExcel = function (context, url, parms) {
        parms = parms || {};
        parms = Utils.prepareDataAjax(parms);

        url = context + url + "?" + $.param(parms);

        window.open(url, '_blank');
    };


    return{
        name: name,
        description: description,
        isObjAvailable: isObjAvailable,
        prepareDataAjax: prepareDataAjax,
        callRequest: callRequest,
        sendFile: sendFile,
        guidGenerator: guidGenerator,
        showLoading: showLoading,
        showNotification: showNotification,
        printReportExcel: printReportExcel
    };
})();