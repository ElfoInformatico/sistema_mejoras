/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* global currentContext, id_usuario, rol */
function alta_nueva_mejora() {
    var confirma = confirm("Desea agregar la mejora?");
    if (confirma) {
        var titulo = $('#titulo').val();
        var select_area = $('#select_area').val();
        var select_proyecto = $('#select_proyecto').val();
        var tipo_mejora = $('#tipo_mejora').val();
        var descripcion_mejora = $('#descripcion_mejora').val();
        $.ajax({
            url: currentContext + "/Improvements",
            data: {accion: "NUEVA_MEJORA", titulo: titulo, select_area: select_area,
                select_proyecto: select_proyecto, tipo_mejora: tipo_mejora,
                descripcion_mejora: descripcion_mejora, usuario: id_usuario},
            type: 'GET',
            async: true,
            dataType: 'JSON',
            success: function (jo)
            {
                console.log(jo);
                swal({
                    title: "Éxito",
                    text: jo.msj,
                    type: "success"
                },
                        function () {
                            location.reload();
                        }
                );
            },
            error: function (xhr, textStatus, errorThrown) {
                var error = xhr.responseText;
                error = error.replace("<br>", "");
                swal({
                    title: "Error en los datos",
                    text: error,
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonClass: "btn-warning",
                    confirmButtonText: "Aceptar",
                    closeOnConfirm: false
                });
            },
            complete: function (data) {
                table.columns.adjust().draw();
            }
        });
    }

    return false;
}
var table;
function llenaTablaMejoras() {

    if (table === undefined) {
        options = {};
        options.columns = [{title: "No."}, {title: "Título"}, {title: "Área"}, {title: "Proyecto"},
            {title: "Descripción"}, {title: "Estado"}, {title: "Fecha"}];

        options.columnDefs = [{targets: [0], visible: false}
        ];
        options.order = [0, "asc"];
        if (rol === 'SISTEMAS') {
            options.columnDefs = [
                {targets: [0], visible: false},
                {targets: [5], "data": null,
                    "defaultContent": '<button class="btn btn-block btn-primary" data-behavior="see-detail">Aceptar Solicitud</button>'},
                {targets: "_all", className: "center"}];
        }
        table = $('#tabla_mejoras').DataTable(options);
    }


    $.ajax({
        url: currentContext + "/Improvements",
        data: {accion: "LLENA_TABLA_MEJORAS", usuario: id_usuario},
        type: 'GET',
        async: true,
        dataType: 'JSON',
        success: function (data)
        {
            table.clear();
            table.rows.add(data);
            table.columns.adjust().draw();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Clave invalida");
        },
        complete: function (data) {
            table.columns.adjust().draw();
        }
    });

}

function carga_elementos_nueva_mejora() {
    $.ajax({
        url: currentContext + "/Improvements",
        data: {accion: "LLENA_SELECT_AREA"},
        type: 'GET',
        async: true,
        dataType: 'JSON',
        success: function (data)
        {
            var select_area = $('#select_area');
            select_area.empty();
            for (var i = 0; i < data.length; i++) {
                select_area.append('<option value=' + data[i].id + '>' + data[i].area + '</option>');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Clave invalida");
        },
        complete: function (data) {
        }
    });
    $.ajax({
        url: currentContext + "/Improvements",
        data: {accion: "LLENA_SELECT_PROYETOS"},
        type: 'GET',
        async: true,
        dataType: 'JSON',
        success: function (data)
        {
            var select_proyecto = $('#select_proyecto');
            select_proyecto.empty();
            for (var i = 0; i < data.length; i++) {
                select_proyecto.append('<option value=' + data[i].id + '>' + data[i].proyecto + '</option>');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Clave invalida");
        },
        complete: function (data) {
        }
    });
}


$(document).ready(function () {
    llenaTablaMejoras();
    carga_elementos_nueva_mejora();
});