<%-- 
    Document   : menu
    Created on : 2/09/2019, 08:51:17 PM
    Author     : chpch
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="jspf/external_login.jspf"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="css/dataTables.bootstrap.css" rel="stylesheet">
        <link href="css/dataTables.jqueryui.css" rel="stylesheet" type="text/css"/>
        <link href="css/dataTables.tableTools.css" rel="stylesheet" type="text/css"/>
        <link href="css/sweetalert.css" rel="stylesheet" type="text/css"/>
        <title>Mejoras Sistemas</title>
    </head>
    <body class="container">
        <h1>Mejoras al Sistema</h1>
        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="" data-toggle="tooltip" data-placement="bottom" title="Inicio" onclick=""><span class="glyphicon glyphicon-home" ></span></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="dropdown" >
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> Existencias </span><span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="" onclick=""><span class="glyphicon glyphicon-globe" aria-hidden="true"></span> Concentrado de Unidades</a></li>
                                <li><a href="" onclick=""><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> Existencias Por Unidad</a></li>

                            </ul>
                        </li>

                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <hr/>
        <div class="row">
            <div class="col-sm-10">
                <h3>
                    Ingresar Mejoras
                </h3>
            </div>
            <div class="col-sm-2">
                <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#alta_usuario">Nueva Mejora</button>
            </div>
        </div>


        <div>
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-bordered table-condensed table-responsive table-striped" id="tabla_mejoras">

                    </table>
                </div>
            </div>
        </div>

        <div id="alta_usuario" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Nueva Mejora</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group">
                                <label for="titulo" class="col-sm-2 control-label">Título</label>
                                <div class="col-sm-10">
                                    <input class="form-control" id="titulo" placeholder="Título" required />
                                </div>
                            </div>
                        </div><br/>
                        <div class="row">
                            <div class="form-group">
                                <label for="select_area" class="col-sm-2 control-label">Área</label>
                                <div class="col-sm-4">
                                    <select class="form-control" id="select_area" required></select>
                                </div>
                                <label for="select_proyecto" class="col-sm-2 control-label">Proyecto</label>
                                <div class="col-sm-4">
                                    <select class="form-control" id="select_proyecto" required></select>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="form-group">
                                <label for="select_proyecto" class="col-sm-2 control-label">Tipo</label>
                                <div class="col-sm-2">
                                    <select class="form-control" id="tipo_mejora" required>
                                        <option value="Mejora">Mejora</option>
                                        <option value="Problema">Problema</option>
                                    </select>
                                </div>
                                <label for="descripcion_mejora" class="col-sm-2 control-label">Descripción</label>
                                <div class="col-sm-6">
                                    <textarea id="descripcion_mejora" class="form-control" required></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-block btn-primary" onclick="alta_nueva_mejora()">Crear</button>
                    </div>
                </div>
            </div>
        </div>


        <script src="js/jquery-2.1.4.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script src="js/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/jquery.dataTables.js"></script>
        <script src="js/dataTables.tableTools.js"></script>
        <script src="js/sweetalert.js" type="text/javascript"></script>
        <script>
                            var id_usuario = "<%=String.valueOf(sesion.getAttribute("idUser"))%>";
                            var rol = "<%=String.valueOf(sesion.getAttribute("rol"))%>";
                            var currentContext = "<c:out value="${pageContext.servletContext.contextPath}" />";
        </script>
        <script src="js/ver_mejoras.js" type="text/javascript"></script>
    </body>
</html>
