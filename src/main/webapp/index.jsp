<%-- 
    Document   : index
    Created on : 2/09/2019, 07:19:51 PM
    Author     : chpch
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/login.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>Mejoras Sistemas</title>
    </head>
    <body class="container">
        <form id="form_index" class="form-signin">
            <h3>Ingreso al Sistema</h3>
            <input class="form-control" placeholder="Usuario" id="usuario" name="usuario" />
            <input class="form-control" placeholder="Contraeña" id="pass" name="pass" />
            <button class="btn btn-block btn-primary" type="button" onclick="login();">Ingresar</button>
        </form>
        <script src="js/jquery-2.1.4.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script src="js/index.js" type="text/javascript"></script>
    </body>
</html>
