/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.medalfa.mejoras_sistema.system;

import javax.servlet.ServletContext;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Permite guardar los parametros del usuario.
 *
 * @author Sebastian
 */
public class UserParameter {

    public static final String PROJECT_ID_PARAMETER = "id";
    public static final String PROJECT_NAME_PARAMETER = "nombre";
    public static final String GOVERNMENT_NAME_PARAMETER = "nombre_gobierno";
    public static final String GOVERNMENT_DESCRIPTION_PARAMETER = "descripcion_gobierno";
    public static final String GOVERNMENT_LOGO_PARAMETER = "logo_gobierno";
    public static final String GOVERNMENT_STARTED_DATE_PARAMETER = "fecha_inicio";
    public static final String PROJECT_DATASOURCE_PARAMETER = "fuente_datos";

    private final JSONObject projects;
    private final JSONArray projectsArray;
    private JSONObject currentProject;

    public UserParameter() {
        this.projects = new JSONObject();
        this.projectsArray = new JSONArray();
    }

    void addProject(JSONObject project) {
        this.projects.put(project.getString(PROJECT_ID_PARAMETER), project);
        this.projectsArray.put(project);
    }

    public void setCurrentProject(String id) {
        this.currentProject = this.projects.getJSONObject(id);
    }

    public JSONArray getProjects() {
        return this.projectsArray;
    }

    public String getIdProject() {
        return this.currentProject.getString(PROJECT_ID_PARAMETER);
    }

    public String getNameProject() {
        return this.currentProject.getString(PROJECT_NAME_PARAMETER);
    }

    public String getGovernmentName() {
        return this.currentProject.getString(GOVERNMENT_NAME_PARAMETER);
    }

    public String getGovernmentDescription() {
        return this.currentProject.getString(GOVERNMENT_DESCRIPTION_PARAMETER);
    }
    
    public String getStartedDate() {
        return this.currentProject.getString(GOVERNMENT_STARTED_DATE_PARAMETER);
    }

    public String getURLPathLogo() {
        return this.currentProject.getString(GOVERNMENT_LOGO_PARAMETER);
    }

    public String getAbsolutePathLogo(ServletContext servletContext) {
        return SystemApp.findAbsolutePath(servletContext,
                this.currentProject.getString(GOVERNMENT_LOGO_PARAMETER));
    }
    
    public String getDataSource(){
        return this.currentProject.getString(PROJECT_DATASOURCE_PARAMETER);
    }
}
