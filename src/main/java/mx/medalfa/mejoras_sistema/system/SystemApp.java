/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.medalfa.mejoras_sistema.system;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletContext;
import static mx.medalfa.mejoras_sistema.system.UserParameter.GOVERNMENT_DESCRIPTION_PARAMETER;
import static mx.medalfa.mejoras_sistema.system.UserParameter.GOVERNMENT_LOGO_PARAMETER;
import static mx.medalfa.mejoras_sistema.system.UserParameter.GOVERNMENT_NAME_PARAMETER;
import static mx.medalfa.mejoras_sistema.system.UserParameter.GOVERNMENT_STARTED_DATE_PARAMETER;
import static mx.medalfa.mejoras_sistema.system.UserParameter.PROJECT_DATASOURCE_PARAMETER;
import static mx.medalfa.mejoras_sistema.system.UserParameter.PROJECT_ID_PARAMETER;
import static mx.medalfa.mejoras_sistema.system.UserParameter.PROJECT_NAME_PARAMETER;
import static mx.medalfa.mejoras_sistema.constants.Qrys.BUSCAR_PROYECTOS;
import org.json.JSONObject;

/**
 * Generar los parametros para el usuario, además guarda los valores por defecto
 * de cada uno de estos.
 *
 * @author Americo
 */
public class SystemApp {

    public static final String COMPANY_LOGO_DEFAULT = "imagen/empresa.png";
    public static final String APP_LOGO_DEFAULT = "imagen/logo_app.png";
    public static final String COMPANY_NAME_DEFAULT = "MEDALFA S.A. DE C.V.";

    public static final String GOVERNMENT_LOGO_DEFAULT = "imagen/gobierno.png";
    public static final String GOVERNMENT_NAME_DEFAULT = "SSM - Secretaría de Salud de Michoacán";
    public static final String GOVERNMENT_DESCRIPTION_DEFAULT = "GOBIERNO DEL ESTADO DE MICHOACÁN <br> Av. Madero, Col Centro, Michoacán, México";
    public static final String GOVERNMENT_STARTED_DATE_DEFAULT = "2018-03-01";
    public static final String ASF_FINISH_DATE_DEFAULT = "2019-12-31";
    public static final String PROJECT_DATASOURCE_DEFAULT = "REPORT_SAA";

    public static final String PROJECT_MEDALFA_ID = "2";
    public static final String PROJECT_ISEM_ID = "1";

    public static UserParameter getCurrentProjects(String projects,
            Connection connection) throws SQLException {

        UserParameter result;

        try (PreparedStatement ps = connection.prepareStatement(
                String.format(BUSCAR_PROYECTOS, projects))) {
            ps.setString(1, GOVERNMENT_NAME_DEFAULT);
            ps.setString(2, GOVERNMENT_DESCRIPTION_DEFAULT);
            ps.setString(3, GOVERNMENT_LOGO_DEFAULT);
            ps.setString(4, GOVERNMENT_STARTED_DATE_DEFAULT);
            ps.setString(5, PROJECT_DATASOURCE_DEFAULT);

            JSONObject project;
            try (ResultSet rs = ps.executeQuery()) {
                result = new UserParameter();

                while (rs.next()) {
                    project = new JSONObject();
                    project.put(PROJECT_ID_PARAMETER, rs.getString(PROJECT_ID_PARAMETER));
                    project.put(PROJECT_NAME_PARAMETER, rs.getString(PROJECT_NAME_PARAMETER));
                    project.put(GOVERNMENT_NAME_PARAMETER, rs.getString(GOVERNMENT_NAME_PARAMETER));
                    project.put(GOVERNMENT_DESCRIPTION_PARAMETER, rs.getString(GOVERNMENT_DESCRIPTION_PARAMETER));
                    project.put(GOVERNMENT_LOGO_PARAMETER, rs.getString(GOVERNMENT_LOGO_PARAMETER));
                    project.put(GOVERNMENT_STARTED_DATE_PARAMETER, rs.getString(GOVERNMENT_STARTED_DATE_PARAMETER));
                    project.put(PROJECT_DATASOURCE_PARAMETER, rs.getString(PROJECT_DATASOURCE_PARAMETER));
                    result.addProject(project);
                }
            }
        }

        return result;
    }

    public static String findAbsolutePath(ServletContext servletContext, String relativeWebPath) {
        return servletContext.getRealPath(String.format("/%s",
                relativeWebPath));
    }
}
