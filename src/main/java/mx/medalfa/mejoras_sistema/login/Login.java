/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.medalfa.mejoras_sistema.login;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import static mx.medalfa.mejoras_sistema.constants.Qrys.INGRESA_USUARIO;
import mx.medalfa.mejoras_sistema.db.ConnectionManager;
import mx.medalfa.mejoras_sistema.db.Source;
import org.json.JSONObject;

/**
 *
 * @author chpch
 */
@WebServlet(name = "Login", urlPatterns = {"/Login"})
public class Login extends HttpServlet {

    public static final String ROL_SUPER_USUARIO_ID = "1";
    public static final String ROL_NORMAL_ID = "2";
    public static final String JURISDICCIONAL_ROL = "3";
    public static final String ROL_FACTURATION_ID = "4";
    public static final String ROL_COORDINADOR_ID = "5";
    public static final String ROL_HOSPITAL_ID = "7";
    public static final String ROL_ADMINISTRADOR_ID = "8";
    public static final String ROL_PROYECTO_ID = "9";
    public static final String ROL_SUPERVISOR_ID = "10";

    public static final int ACTUALIZADA_STATUS = 1;
    public static final int SIN_ACTUALIZAR_STATUS = 2;

    public static final int ACCESO_PERMITIDO_ESTADO = 1;
    public static final int ACCESO_DENEGADO_ESTADO = 0;

    public static final String USER_PARAMETER_FIELD = "userParameters";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.sql.SQLException
     * @throws javax.naming.NamingException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, NamingException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession sesion = request.getSession(true);

        try (Connection connection = ConnectionManager.getManager(Source.REPORT_JAR).getConnection();
                PrintWriter out = response.getWriter();
                PreparedStatement ps = connection.prepareStatement(INGRESA_USUARIO)) {

            JSONObject jo = new JSONObject();
            String mensaje = "ok", status = "ok";
            String usuario = request.getParameter("usuario");
            String pass = request.getParameter("pass");
            ps.setString(1, usuario);
            ps.setString(2, pass);
            try (ResultSet rs = ps.executeQuery()) {

                if (rs.next()) {

                    if (usuario == null) {
                        sesion.invalidate();
                        mensaje = "Datos Incorrectos";
                        status = "fail";
                        registroEntrada(connection, request.getParameter("F_User"), ACCESO_DENEGADO_ESTADO);
                        return;
                    }

                    if (rs.getInt("esta_actualizada") == SIN_ACTUALIZAR_STATUS) {
                        sesion.invalidate();
                        out.println("<script>alert('Su contraseña no esta actualizada. Por favor notifique al administrador. ')</script>");
                        status = "fail";
                        return;
                    }

                    registroEntrada(connection, request.getParameter("usuario"), ACCESO_PERMITIDO_ESTADO);
                    setSessionAttributes(sesion, rs, connection, request);
                } else {
                    mensaje = "Datos Incorrectos";
                    status = "fail";
                }
                //--------------------------------------------------------------

            }
            jo.put("mensaje", mensaje);
            jo.put("status", status);

            out.print(jo);
        } catch (SQLException | NamingException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            try (PrintWriter out = response.getWriter()) {
                out.println("<script>alert('Error interno. Por favor contactar al area de sistemas.')</script>");
            }
        }
    }

    public static HttpSession setSessionAttributes(HttpSession sesion, ResultSet rs,
            Connection connection, HttpServletRequest request) throws SQLException {

        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }

        sesion.setAttribute("nombre", rs.getString("nombre"));
        sesion.setAttribute("usuario", rs.getString("usuario"));
        sesion.setAttribute("rol", rs.getString("rol"));
        sesion.setAttribute("ipRequest", ipAddress);
        sesion.setAttribute("idUser", rs.getInt("idUser"));
        sesion.setAttribute("groupUser", rs.getString("grupoUser"));

        return sesion;
    }

    private void registroEntrada(Connection con, String usuario, int estado) throws SQLException {
        String query = "INSERT INTO registro_entradas (user,status) VALUES (?,?);";
        try (PreparedStatement ps = con.prepareStatement(query)) {
            ps.setString(1, usuario);
            ps.setInt(2, estado);
            ps.executeUpdate();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException | NamingException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException | NamingException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
