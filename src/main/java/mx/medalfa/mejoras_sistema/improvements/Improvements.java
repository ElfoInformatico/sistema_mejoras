/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.medalfa.mejoras_sistema.improvements;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mx.medalfa.mejoras_sistema.constants.Qrys;
import mx.medalfa.mejoras_sistema.db.ConnectionManager;
import mx.medalfa.mejoras_sistema.db.Source;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author chpch
 */
@WebServlet(name = "Improvements", urlPatterns = {"/Improvements"})
public class Improvements extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (Connection con = ConnectionManager.getManager(Source.REPORT_JAR).getConnection(); PrintWriter out = response.getWriter()) {
            String accion = request.getParameter("accion");
            JSONArray json;
            JSONObject jo;
            JSONArray jsona;
            switch (accion) {
                case "NUEVA_MEJORA":
                    jsona = new JSONArray();
                    con.getAutoCommit();
                    try (PreparedStatement localPS = con.prepareStatement(Qrys.INSERT_MEJORA);) {
                        localPS.setString(1, request.getParameter("usuario"));
                        localPS.setString(2, request.getParameter("select_area"));
                        localPS.setString(3, request.getParameter("select_proyecto"));
                        localPS.setString(4, request.getParameter("titulo"));
                        localPS.setString(5, request.getParameter("descripcion_mejora"));
                        localPS.setString(6, "En Espera");
                        localPS.setString(7, request.getParameter("tipo_mejora"));
                        localPS.addBatch();

                        localPS.executeBatch();
                    }
                    con.commit();
                    jo = new JSONObject();
                    jo.put("msj", "Inserción Correcta");
                    jsona.put(jo);
                    out.println(jsona);
                    break;
                case "LLENA_TABLA_MEJORAS":
                    jsona = new JSONArray();
                    try (PreparedStatement localPS = con.prepareStatement(Qrys.GET_MEJORAS);) {
                        localPS.setString(1, request.getParameter("usuario"));
                        try (ResultSet rset = localPS.executeQuery()) {

                            while (rset.next()) {
                                json = new JSONArray();
                                json.put(rset.getString("id"));
                                json.put(rset.getString("titulo"));
                                json.put(rset.getString("area"));
                                json.put(rset.getString("proyecto"));
                                json.put(rset.getString("descripcion"));
                                json.put(rset.getString("estado"));
                                json.put(rset.getString("fecha"));
                                jsona.put(json);
                            }
                        }
                    }
                    out.println(jsona);
                    break;
                case "LLENA_SELECT_AREA":
                    jsona = new JSONArray();
                    try (PreparedStatement localPS = con.prepareStatement(Qrys.GET_AREAS);) {
                        try (ResultSet rset = localPS.executeQuery()) {

                            while (rset.next()) {
                                jo = new JSONObject();
                                jo.put("id", rset.getString("id"));
                                jo.put("area", rset.getString("area"));
                                jsona.put(jo);
                            }
                        }
                    }
                    out.println(jsona);
                    break;
                case "LLENA_SELECT_PROYETOS":
                    jsona = new JSONArray();
                    try (PreparedStatement localPS = con.prepareStatement(Qrys.GET_PROYECTOS);) {
                        try (ResultSet rset = localPS.executeQuery()) {

                            while (rset.next()) {
                                jo = new JSONObject();
                                jo.put("id", rset.getString("id"));
                                jo.put("proyecto", rset.getString("proyecto"));
                                jsona.put(jo);
                            }
                        }
                    }
                    out.println(jsona);
                    break;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Improvements.class.getSimpleName()).log(Level.SEVERE, String.format("m: '%s', sql: '%s'", ex.getMessage(), ex.getSQLState()), ex);
        } catch (NamingException ex) {
            Logger.getLogger(Improvements.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
