/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.medalfa.mejoras_sistema.constants;

/**
 *
 * @author chpch
 */
public class Qrys {

    public static final String INGRESA_USUARIO = "SELECT u.nombre_completo AS `nombre`,u.`user` AS `usuario`,u.rol AS `rol`,u.pass_actualizada+0 `esta_actualizada`,u.id AS idUser,IFNULL(u.grupo,'0') AS grupoUser FROM usuarios AS u WHERE u.`user`=? AND u.pass=MD5(?)";
    public static final String BUSCAR_PROYECTOS = "SELECT p.id, p.nombre, IFNULL(p.nombre_gobierno, ?) AS nombre_gobierno, IFNULL(p.descripcion_gobierno, ?) AS descripcion_gobierno, IFNULL(p.logo_gobierno, ?) AS logo_gobierno, IFNULL(p.fecha_inicio, ?) AS fecha_inicio, IFNULL(p.fuente_datos, ?) AS fuente_datos FROM proyectos AS p WHERE p.id IN (%s);";
    public static final String GET_MEJORAS = "SELECT m.id,m.titulo,a.area,p.proyecto,m.descripcion,m.estado,m.fecha FROM mejoras AS m INNER JOIN areas AS a ON m.area=a.id INNER JOIN proyectos AS p ON m.proyecto=p.id WHERE m.usuario=?";
    public static final String GET_AREAS = "SELECT a.id, a.area FROM areas AS a";
    public static final String GET_PROYECTOS = "SELECT p.id, p.proyecto FROM proyectos AS p";
    public static final String INSERT_MEJORA = "INSERT INTO `mejoras_sistema`.`mejoras`(`id`, `usuario`, `area`, `proyecto`, `titulo`, `descripcion`, `estado`, `fecha`, `tipo`, `hora`) VALUES (0,?,?, ?, ?, ?, ?, NOW(), ?, NOW())";

}
