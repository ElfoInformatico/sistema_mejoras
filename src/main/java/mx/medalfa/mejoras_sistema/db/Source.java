/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.medalfa.mejoras_sistema.db;

/**
 *
 * @author chpch
 */
public enum Source {
    REPORT_JAR("java:comp/env/jdbc/reporteador_jar"),
    REPORT_STADIST("java:comp/env/jdbc/report_stadist"),
    REPORT_SAA("java:comp/env/jdbc/report_saa"),
    REPORT_SAA_ISEM("java:comp/env/jdbc/report_saa_isem"),
    REPORT_SOLUTION("java:comp/env/jdbc/report_solucion"),
    REPORT_CSU("java:comp/env/jdbc/csu_jar");

    public final String text;

    private Source(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return String.format("%s:%s", this.name(), this.text);
    }

}
